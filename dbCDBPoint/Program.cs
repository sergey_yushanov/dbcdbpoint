﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.Globalization;
using System.Threading;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http;
using System.Data.Common;
using System.Threading.Tasks;
using CommandLine;

namespace dbCDBPoint
{
    class Program
    {
        static string url = "192.168.1.21";
        static string port = "9200";
        static string urlElastic = "http://" + url + ":" + port + "/kv-scada-";
        static string urlBulkElastic = "http://" + url + ":" + port + ":/_bulk";
        static string dateTimeFormatSQL = "yyyy-MM-dd HH:mm:ss";
        static string dateFormatConsole = "yyyy-MM-dd";
        static string dateTimeFormatElastic = "yyyy-MM-ddTHH:mm:ss";
        static string dateTimeFormatUrl = "yyyy-MM";
        static int countBulk = 1000;
        static int countTask = 20;
        static int indexPoints = 0;
        static int countPoints = 0;
        static DateTime beginDateTime = new DateTime(2017, 01, 08);
        static DateTime finishDateTime = new DateTime(2017, 01, 09);
        static CultureInfo customCulture;
        static Stopwatch stopWatch;
        static TimeSpan[] tsPrev;

        public static List<Task> taskList = new List<Task>();

        static string connectionStringLocal = @"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename = |DataDirectory|\db.mdf; Integrated Security = True";
        static string connectionStringOdbc = @"DSN=CS";

        class Options
        {
            [Option('b', "begin", Required = true, HelpText = "Begin date")]
            public DateTime beginDateTime { get; set; }

            [Option('f', "finish", Required = true, HelpText = "Finish date")]
            public DateTime finishDateTime { get; set; }

            [Option('p', "port", DefaultValue = "9200", HelpText = "Port on host")]
            public string port { get; set; }

            [Option('h', "host", DefaultValue = "192.168.1.21", HelpText = "Host address")]
            public string host { get; set; }

            [Option('t', "task", DefaultValue = 20, HelpText = "Task count")]
            public int task { get; set; }
        }

        /// <summary>
        /// Класс с полным описанием пойнта
        /// </summary>
        public class Point
        {
            public int id;
            public string fullName;
            public double highHighLimit;
            public double highLimit;
            public double lowLimit;
            public double lowLowLimit;
            public DateTime recordTime;

            public string[] SplitingFullName()
            {
                string[] split = this.fullName.Split(new Char[] { '.', '(', ')', '~' });
                //split = split.Where(val => val != "Волгоград").ToArray();
                split = split.Where((val, idx) => idx != 0).ToArray();
                return split;
            }
        }

        /// <summary>
        /// Класс с полным описанием пойнта из хистори
        /// </summary>
        public class HistoricPoint
        {
            public string recordId;
            //public string value;
            public string formattedValue;
            public double valueAsReal;
            //public int valueAsInteger;
            public DateTime recordTime;
            public int quality;
            public string unit;

            public void SetUnit()
            {
                string[] split = this.formattedValue.Split(new Char[] { ' ' });
                this.unit = split[split.Length - 1];
            }
        }

        /// <summary>
        /// Класс для JSON
        /// </summary>
        public class ElasticPoint
        {
            public int point_id;
            //public string value;
            //public string formatted_value;
            public double value_as_real;
            //public int value_as_integer;
            public string record_time;
            public int quality;
            public double high_high_limit;
            public double high_limit;
            public double low_limit;
            public double low_low_limit;
            public string name;
            public string unit;
            public string[] scada_tags;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="count"></param>
        /// <param name="color"></param>
        /// <param name="text"></param>
        public static void ConsoleWrite(int id, int count, ConsoleColor color, string text)
        {
            int idThread = Thread.CurrentThread.ManagedThreadId;

            TimeSpan tsCur = stopWatch.Elapsed;
            TimeSpan tsDelta = tsCur - tsPrev[idThread];

            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", tsCur.Hours, tsCur.Minutes, tsCur.Seconds, tsCur.Milliseconds / 10);
            string deltaTime = String.Format("{0:00}.{1:00}", tsDelta.Seconds, tsDelta.Milliseconds / 10);

            Console.ForegroundColor = color;

            if (count < 10)
                Console.WriteLine("ThreadId: " + idThread + "\tDelta: " + deltaTime + "\tPointId: " + id + "\t\t" + text + ": " + count + "\t\tTime: " + elapsedTime);
            else
                Console.WriteLine("ThreadId: " + idThread + "\tDelta: " + deltaTime + "\tPointId: " + id + "\t\t" + text + ": " + count + "\tTime: " + elapsedTime);

            tsPrev[idThread] = tsCur;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="recordTime"></param>
        public static void ConsoleWriteFinish(int id, DateTime recordTime)
        {
            int idThread = Thread.CurrentThread.ManagedThreadId;

            TimeSpan tsCur = stopWatch.Elapsed;
            TimeSpan tsDelta = tsCur - tsPrev[idThread];

            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", tsCur.Hours, tsCur.Minutes, tsCur.Seconds, tsCur.Milliseconds / 10);
            string deltaTime = String.Format("{0:00}.{1:00}", tsDelta.Seconds, tsDelta.Milliseconds / 10);

            Console.ForegroundColor = ConsoleColor.Gray;

            Console.WriteLine("ThreadId: " + idThread + "\tDelta: " + deltaTime + "\tPointId: " + id + "\t\tFIN" + "\t\tTime: " + elapsedTime + "\tDate: " + recordTime.ToString(dateFormatConsole, customCulture) + "\t\tPointIdx: " + ++indexPoints + "\t\tPosted: " + countPoints);

            tsPrev[idThread] = tsCur;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseAddress"></param>
        /// <param name="postData"></param>
        /// <param name="id"></param>
        /// <param name="count"></param>
        public static async Task<string> PostAsync(string baseAddress, string postData, int id, int count)
        {
            var sendContent = new StringContent(postData, Encoding.UTF8, "application/json");
            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.PostAsync(baseAddress, sendContent))
            {
                ConsoleWrite(id, count, ConsoleColor.Cyan, "POST");
                return response.ReasonPhrase;
            }
        }
                
        /// <summary>
        /// Запрос для единичной записи в Elastic
        /// </summary>
        /// <param name="historicPoint"></param>
        public static void PutRequest(HistoricPoint historicPoint, Point point)
        {
            ElasticPoint elasticPoint = PrepareElasticData(historicPoint, point);
            string serialized = JsonConvert.SerializeObject(elasticPoint);

            var baseAddress = urlElastic + historicPoint.recordTime.ToString(dateTimeFormatUrl, customCulture) + "/historic/" + historicPoint.recordId + "?pretty";

            countPoints++;
            Console.WriteLine("Id: " + point.id + "\tPOST: 1");

            //Task.Run(async () => await PostAsync(baseAddress, serialized, point.id, 1));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="historicPoint"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static async Task<string> PutRequestAsync(HistoricPoint historicPoint, Point point)
        {
            ElasticPoint elasticPoint = PrepareElasticData(historicPoint, point);
            string serialized = JsonConvert.SerializeObject(elasticPoint);

            var baseAddress = urlElastic + historicPoint.recordTime.ToString(dateTimeFormatUrl, customCulture) + "/historic/" + historicPoint.recordId + "?pretty";

            countPoints++;

            return await PostAsync(baseAddress, serialized, point.id, 1);
        }

        /// <summary>
        /// Запрос для множественной записи в Elastic
        /// </summary>
        /// <param name="historicPoints"></param>
        public static void PutBulkRequest(List<HistoricPoint> historicPoints, Point point)
        {
            string parsedContent = "";
            foreach (HistoricPoint historicPoint in historicPoints)
            {
                parsedContent += PrepareBulkJson(historicPoint, point);
            }
            countPoints += historicPoints.Count;
            //Post(urlBulkElastic, parsedContent, point.id, historicPoints.Count);
            Console.WriteLine("Id: " + point.id + "\tPOST: " + historicPoints.Count);

            //Task.Run(async () => await PostAsync(urlBulkElastic, parsedContent, point.id, historicPoints.Count));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="historicPoints"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static async Task<string> PutBulkRequestAsync(List<HistoricPoint> historicPoints, Point point)
        {
            string parsedContent = "";
            foreach (HistoricPoint historicPoint in historicPoints)
            {
                parsedContent += PrepareBulkJson(historicPoint, point);
            }
            countPoints += historicPoints.Count;

            return await PostAsync(urlBulkElastic, parsedContent, point.id, historicPoints.Count);
        }

        /// <summary>
        /// Подготовка данных для множественного запроса
        /// </summary>
        /// <param name="historicPoint"></param>
        /// <returns></returns>
        public static string PrepareBulkJson(HistoricPoint historicPoint, Point currentPoint)
        {
            string preface = "{ \"index\": { \"_id\": \"" + historicPoint.recordId + "\", \"_index\": \"kv-scada-" + historicPoint.recordTime.ToString(dateTimeFormatUrl, customCulture) + "\", \"_type\": \"historic\"  } }";

            ElasticPoint elasticPoint = PrepareElasticData(historicPoint, currentPoint);
            string serialized = JsonConvert.SerializeObject(elasticPoint);

            return preface + '\n' + serialized + '\n';
        }

        /// <summary>
        /// Создаем запись в базе
        /// </summary>
        /// <param name="queryString">SQL запрос</param>
        /// <param name="connectionString">Источник данных</param>
        private static void InsertCommand(string queryString, string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                int number = command.ExecuteNonQuery();
                Console.WriteLine(string.Format("{0}\t{1}", (number > 0)? "done" : "false", queryString));
            }
        }

        /// <summary>
        /// Обновляем запись в базе
        /// </summary>
        /// <param name="queryString">SQL запрос</param>
        /// <param name="connectionString">Источник данных</param>
        private static void UpdateCommand(string queryString, string connectionString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                int number = command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Читаем все id для пойнтов
        /// </summary>
        /// <param name="queryString"></param>
        /// <param name="connectionString"></param>
        private static void SelectIdsCommand(int startNum, int stopNum)
        {
            OdbcConnection dbConnection = new OdbcConnection("DSN=CS");

            try
            {
                dbConnection.Open();
            }
            catch (OdbcException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            using (SqlConnection connection = new SqlConnection(connectionStringLocal))
            {
                connection.Open();

                string queryString = "SELECT * FROM points ORDER BY Id";
                SqlCommand command = new SqlCommand(queryString, connection);
                SqlDataReader reader = command.ExecuteReader();

                int pointCount = 0;
                while (reader.Read())
                {
                    pointCount++;

                    Point currentPoint = new Point();
                    currentPoint.id = (int)reader[0];
                    currentPoint.fullName = (string)reader[1];
                    currentPoint.highHighLimit = (float)reader[2];
                    currentPoint.highLimit = (float)reader[3];
                    currentPoint.lowLimit = (float)reader[4];
                    currentPoint.lowLowLimit = (float)reader[5];

                    if (pointCount < startNum) continue;
                    if (pointCount > stopNum) break;

                    Console.WriteLine("#:\t\t" + pointCount);
                    Console.WriteLine("Record id:\t" + currentPoint.id);
                    Stopwatch timePerElasticInsert = Stopwatch.StartNew();
                   
                    //ODBCReader();
                    CDBHistoricReader(dbConnection, currentPoint);

                    timePerElasticInsert.Stop();
                    Console.WriteLine("Timer:\t\t{0} sec", timePerElasticInsert.ElapsedMilliseconds / 1000);
                    Console.WriteLine();
                    Console.WriteLine();
                }

                reader.Close();
            }

            dbConnection.Close();
        }

        /// <summary>
        /// Чтение данных из базы SCADA через ODBC подключение
        /// </summary>
        public static void ODBCReader()
        {
            OdbcConnection dbConnection = new OdbcConnection("DSN=CS");

            try
            {
                dbConnection.Open();
            }
            catch (OdbcException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            //CDBPointReader(dbConnection);
            //CPointAlgReader(dbConnection);
            //CDBHistoricReader(dbConnection);

            dbConnection.Close();
        }

        /// <summary>
        /// Чтение из таблицы CDBPoint
        /// </summary>
        /// <param name="dbConnection"></param>
        public static void CDBPointReader(OdbcConnection dbConnection)
        {
            OdbcCommand dbCommand = new OdbcCommand();
            OdbcDataReader dbReader;

            try
            {
                dbCommand = dbConnection.CreateCommand();
                dbCommand.CommandText = "SELECT Id, FullName FROM CDBPoint";
                dbReader = dbCommand.ExecuteReader();

                SaveCDBPoint(dbReader);

                dbReader.Close();
                dbCommand.Dispose();
            }
            catch (OdbcException ex)
            {
                Console.WriteLine("Executing the query '" + dbCommand.CommandText + "' failed.");
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Чтение из таблицы CPointAlg
        /// </summary>
        /// <param name="dbConnection"></param>
        public static void CPointAlgReader(OdbcConnection dbConnection)
        {
            OdbcCommand dbCommand = new OdbcCommand();
            OdbcDataReader dbReader;

            try
            {
                dbCommand = dbConnection.CreateCommand();
                dbCommand.CommandText = "SELECT Id, HighHighLimit, HighLimit, LowLimit, LowLowLimit FROM CPointAlg";
                dbReader = dbCommand.ExecuteReader();

                SaveCPointAlg(dbReader);

                dbReader.Close();
                dbCommand.Dispose();
            }
            catch (OdbcException ex)
            {
                Console.WriteLine("Executing the query '" + dbCommand.CommandText + "' failed.");
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Чтение из таблицы CDBHistoric
        /// </summary>
        /// <param name="dbConnection"></param>
        public static void CDBHistoricReader(OdbcConnection dbConnection, Point currentPoint)
        {
            OdbcCommand dbCommand = new OdbcCommand();
            OdbcDataReader dbReader;

            DateTime currentDateTime = beginDateTime;

            while (!isFinishDateTime(currentDateTime))
            {
                DateTime nextDateTime = currentDateTime.AddDays(1);
                try
                {
                    //Stopwatch timePerSelectHistoric = Stopwatch.StartNew();

                    dbCommand = dbConnection.CreateCommand();
                    String currentDateTimeString = currentDateTime.ToString(dateTimeFormatSQL, customCulture);
                    String nextDateTimeString = nextDateTime.ToString(dateTimeFormatSQL, customCulture);
                    dbCommand.CommandText = "SELECT RecordId, FormattedValue, ValueAsReal, RecordTime, Quality FROM CDBHistoric WHERE Id = " + currentPoint.id + " AND RecordTime >= { ts '" + currentDateTimeString + "' } AND RecordTime < { ts '" + nextDateTimeString + "' }";
                    dbReader = dbCommand.ExecuteReader();

                    SaveBulkCDBHistoric(dbReader, currentPoint);

                    dbReader.Close();
                    dbCommand.Dispose();

                    string updateQuery = "UPDATE points SET RecordTime = { ts '" + currentDateTimeString + "' } WHERE Id = " + currentPoint.id;
                    UpdateCommand(updateQuery, connectionStringLocal);
                    currentDateTime = nextDateTime;
                }
                catch (OdbcException ex)
                {
                    Console.WriteLine("Executing the query '" + dbCommand.CommandText + "' failed.");
                    Console.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// Сохраняем CDBPoint в локальную базу
        /// </summary>
        /// <param name="dbReader"></param>
        public static void SaveCDBPoint(OdbcDataReader dbReader)
        {
            try
            {
                while (dbReader.Read())
                {
                    Point point = new Point();

                    int fCount = dbReader.FieldCount;
                    for (int i = 0; i < fCount; i++)
                    {
                        Object val = dbReader.IsDBNull(i) ? "null" : dbReader.GetValue(i);
                        String fName = dbReader.GetName(i);
                        if (fName.Equals("Id")) point.id = (int)val;
                        if (fName.Equals("FullName")) point.fullName = (string)val;
                    }

                    string insertQuery = string.Format("INSERT INTO points (Id, FullName) VALUES ({0}, N'{1}')", point.id, point.fullName);
                    InsertCommand(insertQuery, connectionStringLocal);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /// <summary>
        /// Сохраняем CPointAlg в локальную базу
        /// </summary>
        /// <param name="dbReader"></param>
        public static void SaveCPointAlg(OdbcDataReader dbReader)
        {
            try
            {
                while (dbReader.Read())
                {
                    Point point = new Point();

                    int fCount = dbReader.FieldCount;
                    for (int i = 0; i < fCount; i++)
                    {
                        Object val = dbReader.IsDBNull(i) ? "null" : dbReader.GetValue(i);
                        String fName = dbReader.GetName(i);
                        if (fName.Equals("Id")) point.id = (int)val;
                        if (fName.Equals("HighHighLimit")) point.highHighLimit = (double)val;
                        if (fName.Equals("HighLimit")) point.highLimit = (double)val;
                        if (fName.Equals("LowLimit")) point.lowLimit = (double)val;
                        if (fName.Equals("LowLowLimit")) point.lowLowLimit = (double)val;
                    }

                    string updateQuery = string.Format("UPDATE points SET HighHighLimit = {0}, HighLimit = {1}, LowLimit = {2}, LowLowLimit = {3} WHERE Id = {4}", point.highHighLimit, point.highLimit, point.lowLimit, point.lowLowLimit, point.id);
                    UpdateCommand(updateQuery, connectionStringLocal);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /// <summary>
        /// Сохраняем CDBHistory в базу Elastic
        /// </summary>
        /// <param name="dbReader"></param>
        public static void SaveCDBHistoric(OdbcDataReader dbReader, Point currentPoint)
        {
            try
            {
                while (dbReader.Read())
                {
                    HistoricPoint historicPoint = new HistoricPoint();

                    int fCount = dbReader.FieldCount;
                    for (int i = 0; i < fCount; i++)
                    {
                        object val = dbReader.IsDBNull(i) ? "null" : dbReader.GetValue(i);
                        string fType = dbReader.GetFieldType(i).Name;
                        string fName = dbReader.GetName(i);

                        if (fName.Equals("RecordId")) historicPoint.recordId = (string)val;
                        //if (fName.Equals("Value")) historicPoint.value = (string)val;
                        if (fName.Equals("FormattedValue")) historicPoint.formattedValue = (string)val;
                        if (fName.Equals("ValueAsReal")) historicPoint.valueAsReal = (double)val;
                        //if (fName.Equals("ValueAsInteger")) historicPoint.valueAsInteger = (int)val;
                        if (fName.Equals("RecordTime")) historicPoint.recordTime = (DateTime)val;
                        if (fName.Equals("Quality")) historicPoint.quality = (int) val;
                    }

                    //currentPoint.recordTime = historicPoint.recordTime;
                    PutRequest(historicPoint, currentPoint);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbReader"></param>
        public static void SaveBulkCDBHistoric(OdbcDataReader reader, Point point)
        {
            try
            {
                List<HistoricPoint> historicPoints = new List<HistoricPoint>();
                int count = 0;
                int displayCount = 0;
                while (reader.Read())
                {
                    HistoricPoint historicPoint = new HistoricPoint();

                    int fCount = reader.FieldCount;
                    for (int i = 0; i < fCount; i++)
                    {
                        object val = reader.IsDBNull(i) ? "null" : reader.GetValue(i);
                        string fType = reader.GetFieldType(i).Name;
                        string fName = reader.GetName(i);

                        if (fName.Equals("RecordId")) historicPoint.recordId = (string)val;
                        //if (fName.Equals("Value")) historicPoint.value = (string)val;
                        if (fName.Equals("FormattedValue")) historicPoint.formattedValue = (string)val;
                        if (fName.Equals("ValueAsReal")) historicPoint.valueAsReal = (double)val;
                        //if (fName.Equals("ValueAsInteger")) historicPoint.valueAsInteger = (int)val;
                        if (fName.Equals("RecordTime")) historicPoint.recordTime = (DateTime)val;
                        if (fName.Equals("Quality")) historicPoint.quality = (int)val;
                    }

                    historicPoints.Add(historicPoint);
                    count++;
                    if (count >= countBulk)
                    {
                        PutBulkRequest(historicPoints, point);
                        historicPoints.Clear();
                        count = 0;
                    }
                    displayCount++;
                }
                if (displayCount == 1) PutRequest(historicPoints.ElementAt(0), point);
                if (displayCount > 1) PutBulkRequest(historicPoints, point);
                reader.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        /// <summary>
        /// Определяем, переходить к следующему Id или нет
        /// </summary>
        /// <returns></returns>
        public static bool isFinishDateTime(DateTime currentDateTime)
        {
            int result = DateTime.Compare(currentDateTime, finishDateTime);
            return result >= 0;
        }

        /// <summary>
        /// Готовим данные в формате JSON
        /// </summary>
        /// <param name="historicPoint"></param>
        /// <returns></returns>
        public static ElasticPoint PrepareElasticData(HistoricPoint historicPoint, Point point)
        {
            ElasticPoint elasticPoint = new ElasticPoint();

            //elasticPoint.value = historicPoint.value;
            //elasticPoint.formatted_value = historicPoint.formattedValue;
            elasticPoint.value_as_real = historicPoint.valueAsReal;
            //elasticPoint.value_as_integer = historicPoint.valueAsInteger;
            elasticPoint.record_time = historicPoint.recordTime.ToString(dateTimeFormatElastic, customCulture);
            elasticPoint.quality = historicPoint.quality;
            historicPoint.SetUnit();
            elasticPoint.unit = historicPoint.unit;

            elasticPoint.point_id = point.id;
            elasticPoint.high_high_limit = point.highHighLimit;
            elasticPoint.high_limit = point.highLimit;
            elasticPoint.low_limit = point.lowLimit;
            elasticPoint.low_low_limit = point.lowLowLimit;
            elasticPoint.scada_tags = point.SplitingFullName();
            elasticPoint.name = elasticPoint.scada_tags[elasticPoint.scada_tags.Length - 1];

            return elasticPoint;
        }


        // ----------------------------------------------------------------------------------------------------

        /// <summary>
        /// Выбираем все пойнты
        /// </summary>
        /// <returns></returns>
        private static List<Point> SelectPoints()
        {
            List<Point> points = new List<Point>();

            using (SqlConnection connection = new SqlConnection(connectionStringLocal))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("SELECT * FROM points ORDER BY Id", connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Point point = new Point();
                    point.id = (int)reader[0];
                    point.fullName = (string)reader[1];
                    point.highHighLimit = (float)reader[2];
                    point.highLimit = (float)reader[3];
                    point.lowLimit = (float)reader[4];
                    point.lowLowLimit = (float)reader[5];
                    if (!reader.IsDBNull(6)) point.recordTime = (DateTime)reader[6];
                    points.Add(point);
                }
                reader.Close();
            }
            return points;
        }

        /// <summary>
        /// Обрабатываем пойнты
        /// </summary>
        /// <param name="points"></param>
        private static void ProcessHistoricRecords(List<Point> points)
        {
            using (OdbcConnection connection = new OdbcConnection(connectionStringOdbc))
            {
                connection.Open();
                DateTime currentDateTime = beginDateTime;

                while (!isFinishDateTime(currentDateTime))
                {
                    DateTime nextDateTime = currentDateTime.AddDays(1);
                    String currentDateTimeString = currentDateTime.ToString(dateTimeFormatSQL, customCulture);
                    String nextDateTimeString = nextDateTime.ToString(dateTimeFormatSQL, customCulture);

                    foreach (Point point in points)
                    {
                        Stopwatch pointWatch = new Stopwatch();
                        pointWatch.Start();

                        TimeSpan ts;
                        string elapsedTime;

                        if (point.recordTime != null && isFinishDateTime(point.recordTime))
                        {
                            ts = stopWatch.Elapsed;
                            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                            Console.WriteLine("Id: " + point.id + "\tFinish: " + point.recordTime.ToString(dateFormatConsole, customCulture) + "\tIndex: " + ++indexPoints + "\tCount: " + countPoints + "\tTime: " + elapsedTime);
                            continue;
                        }

                        TimeSpan ts1 = pointWatch.Elapsed;
                        TimeSpan ts2;
                        TimeSpan ts3;

                        string selectHistoricQuery = "SELECT RecordId, FormattedValue, ValueAsReal, RecordTime, Quality FROM CDBHistoric WHERE Id = " + point.id + " AND RecordTime >= { ts '" + currentDateTimeString + "' } AND RecordTime < { ts '" + nextDateTimeString + "' }";

                        using (OdbcCommand command = new OdbcCommand(selectHistoricQuery, connection))
                        {
                            using (OdbcDataReader reader = command.ExecuteReader())
                            {
                                SaveBulkCDBHistoric(reader, point);

                                //Task.Run(async () => await GetDataReader(command, point));

                                ts2 = pointWatch.Elapsed;

                                //SaveBulkCDBHistoric(reader, point);

                                ts3 = pointWatch.Elapsed;


                                //OdbcCommand command = new OdbcCommand(selectHistoricQuery, connection);
                                //OdbcDataReader reader = command.ExecuteReader();

                                //command.Dispose();

                                string updateQuery = "UPDATE points SET RecordTime = { ts '" + nextDateTimeString + "' } WHERE Id = " + point.id;
                                UpdateCommand(updateQuery, connectionStringLocal);

                                TimeSpan ts4 = pointWatch.Elapsed;

                                ts = stopWatch.Elapsed;
                                elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

                                string elapsed1 = String.Format("{0:00}.{1:00}", ts1.Seconds, ts1.Milliseconds / 10);
                                string elapsed2 = String.Format("{0:00}.{1:00}", ts2.Seconds, ts2.Milliseconds / 10);
                                string elapsed3 = String.Format("{0:00}.{1:00}", ts3.Seconds, ts3.Milliseconds / 10);
                                string elapsed4 = String.Format("{0:00}.{1:00}", ts4.Seconds, ts4.Milliseconds / 10);

                                Console.WriteLine("Id: " + point.id + "\tFinish: " + nextDateTime.ToString(dateFormatConsole, customCulture) + "\tIndex: " + ++indexPoints + "\tCount: " + countPoints + "\tTime: " + elapsedTime + "\t\t" + elapsed1 + "\t" + elapsed2 + "\t" + elapsed3 + "\t" + elapsed4);                                
                            }
                        }
                        pointWatch.Stop();
                    };

                    currentDateTime = nextDateTime;
                }   
            }
        }
        // ----------------------------------------------------------------------------------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private static async Task<HistoricPoint> GetHistoricPoint(DbDataReader reader)
        {
            HistoricPoint historicPoint = new HistoricPoint();
            historicPoint.recordId = await reader.GetFieldValueAsync<string>(0);
            historicPoint.formattedValue = await reader.GetFieldValueAsync<string>(1);
            historicPoint.valueAsReal = await reader.GetFieldValueAsync<double>(2);
            historicPoint.recordTime = await reader.GetFieldValueAsync<DateTime>(3);
            historicPoint.quality = await reader.GetFieldValueAsync<int>(4);
            return historicPoint;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        public static async Task ProcessHistoricRecordsAsync(List<Point> points)
        {

            using (var connection = new OdbcConnection(connectionStringOdbc))
            {
                await connection.OpenAsync();
                DateTime currentDateTime = beginDateTime;

                while (!isFinishDateTime(currentDateTime))
                {
                    DateTime nextDateTime = currentDateTime.AddDays(1);
                    String currentDateTimeString = currentDateTime.ToString(dateTimeFormatSQL, customCulture);
                    String nextDateTimeString = nextDateTime.ToString(dateTimeFormatSQL, customCulture);

                    foreach (Point point in points)
                    {
                        if (point.recordTime != null && isFinishDateTime(point.recordTime))
                        {
                            ConsoleWriteFinish(point.id, point.recordTime);
                            continue;
                        }

                        string selectHistoricQuery = "SELECT RecordId, FormattedValue, ValueAsReal, RecordTime, Quality FROM CDBHistoric WHERE Id = " + point.id + " AND RecordTime >= { ts '" + currentDateTimeString + "' } AND RecordTime < { ts '" + nextDateTimeString + "' }";
                        using (var command = new OdbcCommand(selectHistoricQuery, connection))
                        {
                            using (var reader = await command.ExecuteReaderAsync())
                            {
                                List<HistoricPoint> historicPoints = new List<HistoricPoint>();
                                int count = 0;
                                int displayCount = 0;

                                while (await reader.ReadAsync())
                                {
                                    historicPoints.Add(await GetHistoricPoint(reader));

                                    count++;
                                    if (count >= countBulk)
                                    {
                                        ConsoleWrite(point.id, count, ConsoleColor.Green, "READ");
                                        await PutBulkRequestAsync(historicPoints, point);
                                        historicPoints.Clear();
                                        count = 0;
                                    }
                                    displayCount++;
                                }

                                ConsoleWrite(point.id, count, ConsoleColor.Green, "READ");

                                if (displayCount == 1)
                                    await PutRequestAsync(historicPoints.ElementAt(0), point);
                                if (displayCount > 1)
                                    await PutBulkRequestAsync(historicPoints, point);
                            }
                        }

                        string updateQuery = "UPDATE points SET RecordTime = { ts '" + nextDateTimeString + "' } WHERE Id = " + point.id;
                        UpdateCommand(updateQuery, connectionStringLocal);

                        ConsoleWriteFinish(point.id, nextDateTime);
                    };

                    currentDateTime = nextDateTime;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            customCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture = customCulture;

            var parserResult = Parser.Default.ParseArguments<Options>(args);
            if (parserResult.Errors.Count() > 0) return;

            beginDateTime = parserResult.Value.beginDateTime;
            finishDateTime = parserResult.Value.finishDateTime;
            port = parserResult.Value.port;
            countTask = parserResult.Value.task;
            url = parserResult.Value.host;

            string Path = Environment.CurrentDirectory;
            string[] appPath = Path.Split(new string[] { "bin" }, StringSplitOptions.None);
            AppDomain.CurrentDomain.SetData("DataDirectory", appPath[0]);

            List<Point> points = SelectPoints();

            stopWatch = new Stopwatch();
            stopWatch.Start();

            if (points != null)
            {
                Console.WriteLine("Loaded {0} points", points.Count);
                int countItem = points.Count / countTask;
                int indexItem = 0; 

                List<Point> [] pointListArray = new List<Point> [countTask];
                tsPrev = new TimeSpan[countTask];

                for (int i = 0; i < countTask; i++)
                {
                    if (i + 1 == countTask) countItem = points.Count - indexItem;

                    pointListArray[i] = new List<Point>();
                    pointListArray[i].AddRange(points.GetRange(indexItem, countItem));

                    indexItem += countItem;

                    tsPrev[i] = stopWatch.Elapsed;
                }

                //Parallel.ForEach(pointListArray, (pointList) => {
                //ProcessHistoricRecords(pointList);
                //Task.Run(async () => await ProcessHistoricRecordsAsync(pointList));
                //});

                for (int i = 0; i < countTask; i++)
                {
                    int index = i;
                    taskList.Add(ProcessHistoricRecordsAsync(pointListArray[index]));
                }

                Task.WaitAll(taskList.ToArray());
            }

            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.WriteLine("Posted: " + countPoints + "\tTime: " + elapsedTime);
            Console.WriteLine("Finish, press key for exit");
            Console.ReadKey();
        }
    }
}
